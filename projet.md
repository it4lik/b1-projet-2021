# Projet Infra

## Sommaire

- [Projet Infra](#projet-infra)
  - [Sommaire](#sommaire)
  - [Intro](#intro)
  - [Sujet](#sujet)
    - [Indicateurs de monitoring](#indicateurs-de-monitoring)
    - [Dossiers et bases de données à backup](#dossiers-et-bases-de-données-à-backup)
    - [Bonnes pratiques de sécurité](#bonnes-pratiques-de-sécurité)
  - [Dates](#dates)
  - [Notation](#notation)
    - [Notation continue](#notation-continue)
    - [Oral](#oral)
    - [Ecrit](#ecrit)

## Intro

Le projet infra vise à vous faire mettre en pratique vos compétences en système, infra, réseau et sécurité dans le but de répondre à un besoin réel.

Cela permet de concrétiser les compétences acquises pendant les premiers cours, en se rapprochant d'un cas plus tangible.

Le projet doit se faire en groupe de 2 à 5 personnes et dure ~2 mois.

## Sujet

**Le choix du sujet est libre**, vous devez vous-mêmes apporter votre sujet.

L'idée est la suivante :

- partez d'un vrai besoin, de quelque chose dont vous auriez besoin, dont vous seriez fiers
- discussion autour de la faisabilité du projet
- réalisation du projet, en groupe

> Il sera strictement nécessaire de bosser entre les séances allouées au soutien projet pour rendre quelque chose de correct.

Quelques pistes, des choses qu'on voit régulièrement, liste pas DU TOUT exhaustive :

- serveur de jeu
  - minecraft
  - cs
  - etc
- serveur multimédia
  - streaming audio
  - streaming vidéo
  - hébergement multimédia
- espace de stockage en ligne
  - genre un "cloud" (*berk* ce terme)
- hébergement et sécurisation d'un site web
- monter une infra réseau
  - afin d'aller plus loin et mieux comprendre les notions abordées en cours
  - avec GNS3

Peu importe le sujet choisi...

- les serveurs utilisés devront être monitorés
  - si une vraie solution de monitoring n'est pas en place, il faut au moins remonter [les indicateurs de monitoring](#indicateurs-de-monitoring)
- si la solution comporte des données, elles devront être sauvegardées
  - si une vraie solution de backup n'est pas en place, il faut au moins remonter [les dossiers/base de données que vous auriez backup](#dossiers-et-bases-de-données-à-backup)
- vous écrirez une documentation d'installation sur la mise en place de votre solution
- respect des [bonnes pratiques de sécurité](#bonnes-pratiques-de-sécurité)

### Indicateurs de monitoring

On désigne par ce terme la liste des choses à surveiller, pour l'application de monitoring. Par exemple :

- surveillance de la RAM
- surveillance des cartes réseau
- surveillance de la disponibilité d'un site web
- surveillance de la disponibilité d'une base de données

Ces indicateurs, cette liste, devra figurer dans le rendu écrit (dépôt git) sous forme de tableau par exemple.

### Dossiers et bases de données à backup

Vous devrez faire figurer la liste des dossiers et base de données à backup, en précisant sur quelle machine il/elle se trouve.

On trouvera notamment...

- les dossiers qui contiennent les fichiers de conf liées à votre application
- les dossiers qui contiennent des données liés à votre application

Cette liste figurera dans le compte-rendu écrit (dépôt git).

### Bonnes pratiques de sécurité

Liste non-exhaustive, mais vous devez au moins :

- **gérer vos utilisateurs**
  - vous êtes plusieurs à bosser dans le groupe, et avoir besoin de bosser sur la même machine parfois
  - vous DEVEZ créer un utilisateur par personne
  - vous DEVEZ créer un groupe dont les membres ont le droit de passer des commandes `sudo`
- **gérer les permissions**
  - les fichiers et dossiers que vous gérez doivent avoir les permissions nécessaires pour leur fonctionnement mais pas plus
- **configurer le firewall** de toutes les machines
  - il laisse passer le nécessaire
  - il bloque le reste

## Dates

Le projet Infra YNOV s'étendra sur un peu moins de 2 mois :

|          | Groupe A | Groupe B | Groupe C |
|----------|----------|----------|----------|
| Séance 1 | 04/01    | 04/01    | 31/01    |
| Séance 2 | 11/01    | 11/01    | 18/02    |
| Séance 3 | 17/01    | 17/01    | 03/03    |
| Séance 4 | 18/01    | 18/01    | 10/03    |
| Séance 5 | 01/02    | 01/02    | 17/03    |
| Séance 6 | 08/02    | 08/02    | 21/03    |
| Séance 7 | 15/02    | 15/02    | 24/03    |

**La séance 7 est le jour de l'oral.**

## Notation

### Notation continue

Une note vous sera attribuée en fonction de votre investissement tout au long du projet. Il vous faut :

- tenir le Trello à jour
- avancer de semaine en semaine, sans tout laisser à du travail de dernière minute
- participer aux séances

### Oral

L'oral prendra la forme d'une présentation de ~15/20 min. Elle comprendra :

- présentation du **besoin**
- quel **type de solution** doit être utilisée ?
- quelle **solution spécifique** a été retenue ?
- **pourquoi** cette solution a été retenue ?
- **démonstration**

> Tkt copain, on aura le temps de parler de l'oral plus en détail quand la date approchera.

---

Un exemple, très très bref, pour vous donner une idée :

- besoin : héberger un site web sécurisé en ligne
- type de solution : un serveur web, avec des éléments de sécu
- quelle solution :
  - serveur web Apache
  - reverse proxy NGINX
  - configurations de sécurité sur les serveurs
- pourquoi ?
  - serveur web Apache : libre, opensource, mature, réputé
  - reverse proxy NGINX : libre, opensource, outil de référence, performance, sécurité
- démonstration
  - deux serveurs loués en ligne, l'application est joignable sur `https://web.nom.de.domaine` et elle est fonctionnelle
  - un monitoring élémentaire a été mis en place pour surveiller les ressources des deux serveurs, l'interface est joignable sur `http://monitoring.nom.de.domaine`
  - des sauvegardes du site sont en place, montrer le process de sauvegarde
  - des éléments de sécu additionnels sont en place, montrer les éléménts de sécu

> La démonstration prouve que vous avez répondu au besoin, dans de bonnes conditions. Inutile de montrer tous les fichiers de conf etc, on veut VOIR des trucs qui fonctionnenent. On vous demandera de nous montrer les fichiers de conf si besoin.

### Ecrit

**L'écrit est le dépôt Git.** Il doit comporter au minimum :

- **une documentation d'installation complète** de votre solution
  - si on suit ces instructions, on peut remonter votre solution à l'identique
  - en prérequis, vous mettez "X machines, avec X Go de RAM et X CPU, avec tel OS"
- **un schéma** qui représente les différentes machines impliquées dans la solution
- une section qui explique **comment utiliser la solution**, une fois qu'elle est en place, pour un utilisateur final
  - ça doit être bref
  - "rendez-vous sur `http://tel_adresse` pour accéder à l'application" par exemple
- **indicateurs de monitoring**
  - liste des ressources monitorées
- **backup**
  - liste des dossiers sauvegardés
  - liste des bases de données sauvegardées

> Faire au moins un fichier Markdown pour chacun des points précédents semble être une bonne idée.