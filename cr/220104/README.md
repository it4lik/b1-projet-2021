# Intro

- présentation projet
- présentation sujet
- objectifs séance :
  - consituter groupes
  - trouver sujets
  - créer le Trello/Git

# Groupe 1

- matin
- tout de suite plein d'idées, comme d'hab
- trading web app
  - intéressés finance
  - sexy frontpage
  - consommer API pour récup data
- pb soulevées :
  - stocker données ?
  - sécu données ? 
  - protection site web ?
  - architecture ?

# Groupe 2

- matin
- idée fast
- webapp
- marketplace : users, data donc db, donc sécu
- infra autour, à voir monit, backup, rp

# Groupe 3

- matin
- serveur de jeu dédié GTA5
  - https://docs.fivem.net/docs/server-manual/server-commands/
  - compilé pour winwin ? :(
- industrialisation, rcon, monit, backup
- premiète étape : le faire tourner doit être une formalité

# Groupe 4

- matin
- ethan a déjà un site pour mangas, ils veulent transposer aux animes
- serveur de streaming vidéo
  - animes
  - base de données d'animes
- API, base de données, backup
- online hosting
- right now : 
  - dev beta
  - dev API

# Groupe 5

- matin
- assez fast, 3 dévs
- webapp qui présente des fiches de célébrités
- db, online hosting, sécu, API dev
- aucun skill en web, trkl, petiapeti

# Groupe 6

- matin
- webapp industrialisation serv minecraft
- hébergement, API
- sauvegarde auto 

# Groupe 7

- matin
- pas trop de skill serv minecraft
- premier temps : maitriser l'install
- deuxième temps : script industrialisation

# Groupe 8

- matin
- webapp (comparateur prix/svc downloader reportages etc)
- django ou node
- db, secu, API

# Groupe 9

- matin
- industrialisation minecraft serv
- restriction de ressources

# Groupe 10

- aprem
- maîtriser install minecraft
- industrialiser fourniture service
- webui + rcon 
- monitoring
- backup
- self hosted

# Groupe 11

- aprem
- 

# Groupe 12

- aprem

# Groupe 13

- aprem
- client léger
  - VM avec serveur VNC
  - propose un certain nombre de services
- app web maison de jeu de cartes

# Groupe 14

- aprem

# Groupe 15

- aprem
- classical webapp
  - plusieurs idées, toutes des webapps
- écosystème classique autour
- self hosted ?

# Groupe 16

- classical webapp
- consommation de clopes, stats, etc

# Groupe 17

- ils avaient 6 idées de projet
  - webapps enfait
- new project :
  - faire 1, 2 ou les 6 webapps, mais très fast, très sommaire
  - industrialiser la création de vm préconfigurées
  - déposer conf automatiquement
