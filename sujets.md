| Number | Membres | Projet | Git | Trello |
| --- | --- | --- | --- | --- |
| 1 | Rogard, Gachassin | Trading app | https://github.com/HyouKash/spacecrypto.me | https://trello.com/invite/spacecrypto1/ae00655077be0e8d227aeb619021a3c7 |
| 2 | Coutant, Taraire, Toulemon, Duniaud | Marketplace web app | https://gitlab.com/mathiascoutant/marketplace/ | https://trello.com/invite/b/CRfjBqVe/f8e2d7854c41bbf0f2cd0502fdd2402d/market |
| 3 | Beguerie, Pougeard, Labasse, Dubaquier | GTA5 dedicated server | x | x |
| 4 | Alvez, Moraud | Streaming vidéo animes |  https://github.com/ImShEthan/Animes-origines | https://trello.com/b/JAaCAGXw/projet-ynov |
| 5 | Corre, Grand-Morcel, Vayssier | Webapp stars | https://github.com/leo-grandmorcel/Projet-M-diaCenter | https://trello.com/b/r8xOYL6z/conduite-de-projet |
| 6 | Lozac, Jovanovich, Bonomo | Minecraft servers + webapp | https://try.gitea.io/Gene/LinuX | x |
| 7 | Machy, Girod, Portier | Minecraft servers | https://github.com/Enrick1234/ServeurMinecraft.git | https://trello.com/invite/b/z3DwmCI0/b015e5c44d65800f24d9f1da74dc6f27/serveur-minecraft |
| 8 | Porte, Bourmaud | Web prix ou svc dl | https://gitlab.com/Hbourmaud/projetcomparateur.git | https://trello.com/b/DMwwuOJ8 |
| 9 | Duperau, Garrabos, Selva, Ferreira | Minecraft server | https://github.com/AFERREIRA33/Projet-infra | https://trello.com/invite/b/ys7UdcmC/e3dd9a8ea4d84d3dc37961d29ed6da75/love-staline |
| 10 | Magne, Garcia, Dalamel De Bournet, Gilles | Minecraft | https://gitlab.com/Lukabdx/projet-infra-b1 | https://trello.com/b/NH9bLsoX  |
| 11 | Vidalie, Chalaye, Bescond | VPN saas |  https://gitlab.com/Ceduss/projet-infra/-/tree/main | https://trello.com/b/t0H3y3ap/projet-infra |
| 12 | Martinez Bienabe, Bravo, Koerbert-Nathan | VPN | https://gitlab.com/Promethiyas/infra/-/tree/main
 | x |
| 13 | Renzema, Pajak, Durand, Gaillard | Client léger + accès à un service de jeu de cartes en ligne | x | x |
| 14 | Bousseria, Lavaud | x | https://github.com/Chenishere/Projet-infra  | https://trello.com/invite/b/4vJpjCsD/9db0098f89af0b5b7737c7f2a58aae26/projet |
| 15 | Eveillard, Eymas, Rivry, Ruemeli | Webapp | https://gitlab.com/Fireginger/projet-infra-b1-ynov | https://trello.com/b/NdHvaoq0/travail-groupe-application-web |
| 16 | Vaisseur, Roulland, Laroumanie | Webapp | https://github.com/KeunotorCagoule/Projet-Infra.git | https://trello.com/b/KkzRz2Wq/avancement |
| 17 | Aloui, Revol, Gadebille | Industrialiser fourniture service web | https://github.com/baptistegdb/projet-infra-B1/ | https://trello.com/b/KC99r5K7/projet-infra-b1 |
| 18 | Senhaj, Brun | Snake D Arena | https://github.com/charles-brun/projetinfra | https://trello.com/invite/b/b487Qj7Y/ca53600bb881e7761f1584c8f3abf607/projet-infra |
| 19 | Garcia, Maurizi | Infra réseau | https://github.com/fg33140/ProjetInfra.git | https://trello.com/invite/b/EQ1JJL5e/556d0fbc8a8e3916481049acad499ee5/infra-r%C3%A9seau |
| 20 | Doublait, Latorre, Pietrzak | Site web | https://github.com/Max33270/Projet-Infra.git | https://trello.com/invite/b/1AsQBkND/29308438d60ab8592e3569efb91aacf6/projet-infra-server-web-sites |
| 21 | Weber, Menard, Moïska | Music streaming + smart upload |https://github.com/GammaRay99/streaming_music_YNOV.git | https://trello.com/b/OEtPciew/org-infra |
| 22 | Wora Kingbell, Zitouni, Batguzer | Video streaming + smart search and upload | | |
| 23 | Toribio, Thebault | mc serv | | |
| 24 | Poirier, Bouriat | rtmp + écosystème autour | https://github.com/Pierre-Herve/Private_stream_service.git | |
| 25 | Laville, Missier | mc serv | | |
| 26 | Bedja, Leconte | pw mgr | https://gitlab.com/Thhhe0/projet-infra-gestionnaire-mdp | https://trello.com/b/uW40Mvuc/todolist |
| 27 | Biret, Sengul, Juillard | VPN | https://gitlab.com/DawnWT/projet-infra-vpn | https://trello.com/b/DTZ6j5Ol/vpn |
